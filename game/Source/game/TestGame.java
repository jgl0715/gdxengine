package game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;

import me.joeyleavell.gdxengine.core.EngineLauncher;
import me.joeyleavell.gdxengine.core.EngineMain;
import me.joeyleavell.gdxengine.core.Game;
import me.joeyleavell.gdxengine.core.InputSource;
import me.joeyleavell.gdxengine.core.InputSource.SourceType;
import me.joeyleavell.gdxengine.event.Event;
import me.joeyleavell.gdxengine.event.EventListener;
import me.joeyleavell.gdxengine.scene.GameWorld;

public class TestGame extends Game
{

	public static boolean startAsClient = false;
	public static String serverIp;
	public static int serverPort;

	private GameWorld world;

	public TestGame()
	{
		super();
	}

	@Override
	public void create()
	{
		// Register game objects
		EngineMain.getObjectManager().addGameObject(0, Player.class);
		EngineMain.getObjectManager().addGameObject(1, Wall.class);

		// Register input actions
		EngineMain.getInputManager().registerAction("Shoot");
		EngineMain.getInputManager().registerActionBinding("Shoot", new InputSource(SourceType.MOUSE, Buttons.LEFT));

		// Clients can create their own world and player. The server will get notified
		// of player creation if this is a client so no need to do a server check here.
		world = new GameWorld();
		EngineMain.setCurrentWorld(world);

		// Setup client or server depending on specified options
		if (startAsClient)
			EngineMain.getNetworkManager().connectToServer(serverIp, serverPort);
		else
			EngineMain.getNetworkManager().runServer(serverPort);

		// Add the platforms
		if (EngineMain.getNetworkManager().isServer())
		{
			float w = Gdx.graphics.getWidth() - 1;
			float h = 10;

			world.addObjectWithReplication(new Wall(world, w / 2 + 1, h / 2 + 1));
			world.addObjectWithReplication(new Wall(world, w / 2 + 1, Gdx.graphics.getHeight() - h / 2 + 1));

			world.addObjectWithReplication(new Player(world, 5, Gdx.graphics.getHeight() / 2));

		} else
		{
			world.addObjectWithReplication(new Player(world, Gdx.graphics.getWidth() - 5, Gdx.graphics.getHeight() / 2));
		}

		EngineMain.getNetworkManager().getServerManager().getClientJoinedDelegate().addListener(new EventListener()
		{
			@Override
			public void fire(Event event)
			{
				// add ball
			}
		});
	}

	@Override
	public void update(float delta)
	{
		if (Gdx.input.isKeyJustPressed(Keys.E) && EngineMain.getNetworkManager().isServer())
		{
			// world.addObjectWithReplication(new DynamicBox(world, Gdx.input.getX(),
			// Gdx.graphics.getHeight() - Gdx.input.getY()));
		}
	}

	@Override
	public void render()
	{
	}

	@Override
	public void dispose()
	{

	}

	public static void main(String[] args)
	{

		startAsClient = false;
		serverPort = 25565;

		switch (args.length)
		{
		case 2:
			if (args[0].equalsIgnoreCase("-s"))
				serverPort = Integer.parseInt(args[1]);
			else
				System.err.println("Invalid option");
			break;
		case 3:
			if (args[0].equalsIgnoreCase("-c"))
			{
				startAsClient = true;
				serverIp = args[1];
				serverPort = Integer.parseInt(args[2]);
			} else
				System.err.println("Invalid option");
			break;
		default:
			System.err.println("Too many arguments!");
			break;
		}

		EngineLauncher.launchDesktop(new TestGame());
	}

}
