package me.joeyleavell.gdxengine.collision;

import me.joeyleavell.gdxengine.math.Vector2f;

public class Box
{

	private Vector2f position;
	private float w;
	private float h;

	public Box(Vector2f position, float w, float h)
	{
		this.position = position;
		this.w = w;
		this.h = h;
	}
	
	public void set(Vector2f position, float w, float h)
	{
		this.position = position;
		this.w = w;
		this.h = h;
	}

	public Vector2f getPosition()
	{
		return position;
	}

	public float getWidth()
	{
		return w;
	}

	public float getHeight()
	{
		return h;
	}

	public void setWidth(float w)
	{
		this.w = w;
	}

	public void setHeight(float h)
	{
		this.h = h;
	}

	public boolean collisionTest(Box other)
	{
		return collisionTest(other, 0, 0);
	}

	public boolean collisionTest(Box other, float dx, float dy)
	{
		float nx = position.x + dx;
		float ny = position.y + dy;

		if ((nx + w >= other.position.x && nx + w < other.position.x + other.w) || (nx >= other.position.x && nx < other.position.x + other.w))
			if ((ny + h >= other.position.y && ny + h < other.position.y + other.h) || (ny >= other.position.y && ny < other.position.y + other.h))
				return true;

		return false;

	}

}
