package me.joeyleavell.gdxengine.event;

import java.util.ArrayList;
import java.util.List;

public class MulticastDelegate
{

	private List<EventListener> listeners;

	public MulticastDelegate()
	{
		listeners = new ArrayList<EventListener>();
	}

	public void addListener(EventListener listener)
	{
		listeners.add(listener);
	}
	
	public void removeListener(EventListener listener)
	{
		listeners.remove(listener);
	}

	public void fire(Event event)
	{
		for (EventListener listener : listeners)
			listener.fire(event);
	}

}
