package me.joeyleavell.gdxengine.math;

public class Vector2f
{

	public float x;
	public float y;

	public Vector2f()
	{
		this.x = 0;
		this.y = 0;
	}

	public Vector2f(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public Vector2f(Vector2f other)
	{
		this.x = other.x;
		this.y = other.y;
	}

	public void set(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public void set(Vector2f other)
	{
		this.x = other.x;
		this.y = other.y;
	}

	public void addTo(float x, float y)
	{
		this.x += x;
		this.y += y;
	}

	public void addTo(Vector2f other)
	{
		this.x += other.x;
		this.y += other.y;
	}

	public Vector2f add(float x, float y)
	{
		return new Vector2f(this.x + x, this.y + y);
	}

	public Vector2f add(Vector2f other)
	{
		return new Vector2f(this.x + other.x, this.y + other.y);
	}

	public void subtractFrom(float x, float y)
	{
		this.x -= x;
		this.y -= y;
	}

	public void subtractFrom(Vector2f other)
	{
		this.x -= other.x;
		this.y -= other.y;
	}

	public Vector2f subtract(float x, float y)
	{
		return new Vector2f(this.x - x, this.y - y);
	}

	public Vector2f subtract(Vector2f other)
	{
		return new Vector2f(this.x - other.x, this.y - other.y);
	}

	public boolean equals(Object other)
	{
		if (other instanceof Vector2f)
		{
			Vector2f otherVector = (Vector2f) other;
			return otherVector.x == x && otherVector.y == y;
		}
		return false;
	}

	@Override
	public String toString()
	{
		return "[" + x + ", " + y + "]";
	}

}
