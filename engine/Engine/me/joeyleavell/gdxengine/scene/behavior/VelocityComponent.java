package me.joeyleavell.gdxengine.scene.behavior;

import me.joeyleavell.gdxengine.math.Vector2f;
import me.joeyleavell.gdxengine.scene.data.DataComponentVector2f;
import me.joeyleavell.gdxengine.scene.object.GameObject;

public class VelocityComponent extends BehaviorComponent
{

	public VelocityComponent(GameObject parent, String name, boolean updates, boolean renders)
	{
		super(parent, name, updates, renders);

		addDataComponent(DataComponentVector2f.class, GameObject.POSITION);
		addDataComponent(DataComponentVector2f.class, GameObject.VELOCITY);
	}

	@Override
	public void update(float delta)
	{
		super.update(delta);

		if (getParent().isAuthoritative())
		{
			Vector2f pos = getDataComponent(DataComponentVector2f.class, GameObject.POSITION).value;
			Vector2f vel = getDataComponent(DataComponentVector2f.class, GameObject.VELOCITY).value;

			pos.addTo(vel);
			
//			getDataComponent(DataComponentVector2f.class, GameObject.POSITION).changed();
		}
	}

}
