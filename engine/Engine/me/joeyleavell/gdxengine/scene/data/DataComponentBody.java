package me.joeyleavell.gdxengine.scene.data;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import me.joeyleavell.gdxengine.event.Event;
import me.joeyleavell.gdxengine.event.EventListener;
import me.joeyleavell.gdxengine.scene.GameWorld;
import me.joeyleavell.gdxengine.scene.object.GameObject;

public class DataComponentBody extends DataComponent
{

	public Body value;

	public DataComponentBody(GameObject parent, String name, BodyDef.BodyType type)
	{
		super(parent, name, true, false, false, false);

		BodyDef bDef = new BodyDef();

		bDef.active = true;
		bDef.allowSleep = false;
		bDef.angle = 0.0f;
		bDef.angularDamping = 0.0f;
		bDef.angularVelocity = 0.0f;
		bDef.awake = true;
		bDef.bullet = false;
		bDef.fixedRotation = true;
		bDef.gravityScale = 1.0f;
		bDef.linearDamping = 1.0f;
		bDef.linearVelocity.set(0, 0);
		bDef.position.set(0, 0);
		bDef.type = type;

		parent.getOnReadSpawnDelegate().addListener(new EventListener()
		{
			@Override
			public void fire(Event event)
			{
				// Need to teleport the physics object to the location transferred over the
				// network
				teleport();
			}
		});

		value = parent.getWorld().getPhysicsWorld().createBody(bDef);
	}

	public void createBoxFixture(float w, float h)
	{
		FixtureDef fDef = new FixtureDef();
		PolygonShape polygon = new PolygonShape();

		polygon.setAsBox((w / 2) / GameWorld.PIXELS_PER_METER, (h / 2) / GameWorld.PIXELS_PER_METER);

		fDef.density = 0.1f;
		fDef.friction = 1.0f;
		fDef.isSensor = false;
		fDef.restitution = 0.1f;
		fDef.shape = polygon;

		value.createFixture(fDef);
	}

	/**
	 * Pulls the current location from the position component and teleports the
	 * physics object to that location.
	 */
	public void teleport()
	{
		DataComponentVector2f positionComponent = getParent().findDataComponentByClassAndName(DataComponentVector2f.class, GameObject.POSITION);

		value.setTransform(positionComponent.value.x / GameWorld.PIXELS_PER_METER, positionComponent.value.y / GameWorld.PIXELS_PER_METER, value.getTransform().getRotation());
	}

}
