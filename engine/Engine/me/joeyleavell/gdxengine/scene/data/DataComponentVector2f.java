package me.joeyleavell.gdxengine.scene.data;

import java.io.IOException;

import com.badlogic.gdx.utils.DataInput;
import com.badlogic.gdx.utils.DataOutput;

import me.joeyleavell.gdxengine.math.Vector2f;
import me.joeyleavell.gdxengine.scene.object.GameObject;

public class DataComponentVector2f extends DataComponent
{

	public final Vector2f value;

	public DataComponentVector2f(GameObject parent, String name, boolean serialized, boolean replicated, boolean clientAuthoratative)
	{
		super(parent, name, true, serialized, replicated, clientAuthoratative);

		value = new Vector2f();
	}

	@Override
	public void send(DataOutput output)
	{
		try
		{
			output.writeFloat(value.x);
			output.writeFloat(value.y);
		} catch (IOException e)
		{
			System.err.println("Could not serialize Vector2f value " + getName());
			e.printStackTrace();
		}
	}

	@Override
	public void read(DataInput input)
	{
		try
		{
			value.set(new Vector2f(input.readFloat(), input.readFloat()));
		} catch (IOException e)
		{
			System.err.println("Could not deserialize Vector2f value " + getName());
			e.printStackTrace();
		}
	}

}
