package me.joeyleavell.gdxengine.scene.data;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.badlogic.gdx.utils.DataInput;
import com.badlogic.gdx.utils.DataOutput;

import me.joeyleavell.gdxengine.event.MulticastDelegate;
import me.joeyleavell.gdxengine.network.Packets;
import me.joeyleavell.gdxengine.scene.object.GameObject;

public abstract class DataComponent
{

	private int dataId;
	private String name;
	private GameObject parent;
	private boolean serialized;
	private boolean replicated;
	private boolean locallyControlled;
	private boolean mutable;

	/**
	 * The replication rate of this data component in milliseconds.
	 */
	private double replicationRate;
	private double lastReplication;

	private Field valueField;
	private Class<?> copyCtorParameter;
	private Object valueFieldValue;
	private Object lastValueFieldValue;

	private MulticastDelegate onRep;

	public DataComponent(GameObject parent, String name, boolean mutable, boolean serialized, boolean replicated, boolean locallyControlled)
	{
		this.name = name;
		this.parent = parent;
		this.serialized = serialized;
		this.replicated = replicated;
		this.locallyControlled = locallyControlled;

		// Set default replication rate
		replicationRate = (1000.0 / 60.0);
		lastReplication = System.currentTimeMillis();
		
		onRep = new MulticastDelegate();

		// None of this logic applies if the component isn't replicated
		if (replicated)
		{
			try
			{
				valueField = getClass().getField("value");

				// Assume default constructor
				if (mutable)
					lastValueFieldValue = valueField.getType().getConstructor().newInstance();

				copyCtorParameter = valueField.getType();
			} catch (IllegalArgumentException e)
			{
				e.printStackTrace();
			} catch (IllegalAccessException e)
			{
				e.printStackTrace();
			} catch (NoSuchFieldException e)
			{
				e.printStackTrace();
			} catch (SecurityException e)
			{
				e.printStackTrace();
			} catch (InstantiationException e)
			{
				e.printStackTrace();
			} catch (InvocationTargetException e)
			{
				e.printStackTrace();
			} catch (NoSuchMethodException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public MulticastDelegate getOnRepDelegate()
	{
		return onRep;
	}

	public GameObject getParent()
	{
		return parent;
	}

	public String getName()
	{
		return name;
	}

	public void setDataId(int dataId)
	{
		this.dataId = dataId;
	}

	public int getDataId()
	{
		return dataId;
	}

	public boolean isReplicated()
	{
		return replicated;
	}

	public void setCopyCtorParameter(Class<?> copyCtorParameter)
	{
		this.copyCtorParameter = copyCtorParameter;
	}

	public void initValueField()
	{

		try
		{
			// Initialize the state of the last value
			valueFieldValue = valueField.get(this);
			if (mutable)
			{
				// object.set(otherObject)
				Method setMethod = lastValueFieldValue.getClass().getMethod("set", lastValueFieldValue.getClass());
				setMethod.invoke(lastValueFieldValue, valueFieldValue);
			} else
			{
				// Make a copy of valueFieldValue if not mutable using copy constructor

				lastValueFieldValue = valueField.getType().getConstructor(copyCtorParameter).newInstance(valueFieldValue);
			}
		} catch (NoSuchMethodException e)
		{
			e.printStackTrace();
		} catch (SecurityException e)
		{
			e.printStackTrace();
		} catch (IllegalAccessException e)
		{
			e.printStackTrace();
		} catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		} catch (InvocationTargetException e)
		{
			e.printStackTrace();
		} catch (InstantiationException e)
		{
			e.printStackTrace();
		}
	}

	public boolean canReplicate()
	{
		if (isReplicated() && (System.currentTimeMillis() - lastReplication >= replicationRate) && isChanged())
		{
			lastReplication = System.currentTimeMillis();
			return true;
		}
		return false;
	}

	public boolean isChanged()
	{
		return !lastValueFieldValue.equals(valueFieldValue);
	}

	public boolean isSerialized()
	{
		return serialized;
	}

	public boolean isLocallyControlled()
	{
		return locallyControlled;
	}

	public void replicate(DataOutput output)
	{
		// [Replication] [EntityId] [DataId] [DataValue]

		try
		{
			synchronized (output)
			{
				output.writeInt(Packets.REPLICATION);
				output.writeInt(parent.getId());
				output.writeInt(dataId);
				send(output);
			}

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void send(DataOutput output)
	{

	}

	public void read(DataInput input)
	{

	}

}
