package me.joeyleavell.gdxengine.scene.object;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import me.joeyleavell.gdxengine.core.EngineMain;
import me.joeyleavell.gdxengine.event.Event;
import me.joeyleavell.gdxengine.event.EventListener;
import me.joeyleavell.gdxengine.math.Vector2f;
import me.joeyleavell.gdxengine.scene.GameWorld;
import me.joeyleavell.gdxengine.scene.behavior.PhysicsComponent;
import me.joeyleavell.gdxengine.scene.data.DataComponentBody;
import me.joeyleavell.gdxengine.scene.data.DataComponentFloat;
import me.joeyleavell.gdxengine.scene.data.DataComponentVector2f;

public class GOBox extends GameObject
{

	public GOBox(GameWorld gameWorld, BodyType type)
	{
		super(gameWorld);

		DataComponentBody bodyComponent = new DataComponentBody(this, BOUNDS, type);
		DataComponentVector2f sizeComponent = new DataComponentVector2f(this, SIZE, false, true, false);
		DataComponentFloat rotationComponent = new DataComponentFloat(this, ROTATION, true, true, false);

		addDataComponent(bodyComponent);
		addDataComponent(rotationComponent);
		addDataComponent(sizeComponent);

		// Add a physics component
		addBehaviorComponent(new PhysicsComponent(this, "Physics"));
		

	}

	public GOBox(GameWorld gameWorld, float w, float h, BodyType type)
	{
		this(gameWorld, type); 

		findDataComponentByClass(DataComponentBody.class).createBoxFixture(w, h);
		findDataComponentByClassAndName(DataComponentVector2f.class, SIZE).value.set(w, h);
	}

	@Override
	public void render()
	{
		super.render();

		if (renderSettings.visible)
		{
			switch (renderSettings.mode)
			{
			case FRAME:

				ShapeRenderer sr = EngineMain.getShapeRenderer();
				Vector2f position = findDataComponentByClassAndName(DataComponentVector2f.class, POSITION).value;
				Vector2f size = findDataComponentByClassAndName(DataComponentVector2f.class, SIZE).value;
				float rotation = findDataComponentByClassAndName(DataComponentFloat.class, ROTATION).value;
				float width = size.x;
				float height = size.y;

				sr.begin(ShapeType.Line);
				sr.rect(position.x - width / 2, position.y - height / 2, width / 2, height / 2, width, height, 1.0f, 1.0f, rotation);
				sr.end();

				break;
			default:
				break;
			}
		}

	}

}
