package me.joeyleavell.gdxengine.scene.object;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.utils.DataInput;
import com.badlogic.gdx.utils.DataOutput;

import me.joeyleavell.gdxengine.core.EngineMain;
import me.joeyleavell.gdxengine.core.RenderSettings;
import me.joeyleavell.gdxengine.event.Event;
import me.joeyleavell.gdxengine.event.MulticastDelegate;
import me.joeyleavell.gdxengine.network.Packets;
import me.joeyleavell.gdxengine.scene.GameWorld;
import me.joeyleavell.gdxengine.scene.behavior.BehaviorComponent;
import me.joeyleavell.gdxengine.scene.data.DataComponent;
import me.joeyleavell.gdxengine.scene.data.DataComponentVector2f;

public class GameObject
{

	// Common component names
	public static final String POSITION = "Position";
	public static final String VELOCITY = "Velocity";
	public static final String SIZE = "Size";
	public static final String ROTATION = "Rotation";
	public static final String ACCELERATION = "Acceleration";
	public static final String BOUNDS = "Bounds";

	private volatile static int nextId = -1;

	protected volatile int id;
	protected boolean authoritative;
	protected GameWorld world;
	protected RenderSettings renderSettings;
	protected boolean ownedLocally;
	private int nextDataComponent;
	private List<DataComponent> dataComponents;
	private List<BehaviorComponent> behaviorComponents;
	private boolean dead;

	private MulticastDelegate onReadSpawnDelegate;
	private MulticastDelegate onSendSpawnDelegate;

	/**
	 * All game objects must contain this constructor.
	 * 
	 * @param world
	 *            The world that the game object will be spawned in.
	 * @param server
	 *            Whether or not the game instance this game object is running on is
	 *            a server.
	 */
	public GameObject(GameWorld world)
	{
		// Only increment ID if this is the server
		if (EngineMain.getNetworkManager().isServer())
			++nextId;

		this.id = nextId;
		this.authoritative = EngineMain.getNetworkManager().isServer();
		this.world = world;
		this.renderSettings = new RenderSettings();
		this.nextDataComponent = 0;
		this.ownedLocally = true;
		this.dataComponents = new ArrayList<DataComponent>();
		this.behaviorComponents = new ArrayList<BehaviorComponent>();
		this.dead = false;

		this.onReadSpawnDelegate = new MulticastDelegate();
		this.onSendSpawnDelegate = new MulticastDelegate();

		// Generate default components
		addPositionDataComponent();
	}

	public boolean hasId()
	{
		return id > -1;
	}

	public boolean isDead()
	{
		return dead;
	}

	public MulticastDelegate getOnReadSpawnDelegate()
	{
		return onReadSpawnDelegate;
	}

	public MulticastDelegate getOnSendSpawnDelegate()
	{
		return onSendSpawnDelegate;
	}

	public void remove()
	{
		dead = true;
	}

	public void setLocallyOwned(boolean locallyOwned)
	{
		this.ownedLocally = false;
	}

	public void setOwnedLocally(boolean ownedLocally)
	{
		this.ownedLocally = ownedLocally;
	}

	public boolean isOwnedLocally()
	{
		return ownedLocally;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getId()
	{
		return id;
	}

	public boolean isAuthoritative()
	{
		return authoritative;
	}

	public void setAuthoritative(boolean authoritative)
	{
		this.authoritative = authoritative;
	}

	public GameWorld getWorld()
	{
		return world;
	}

	public void addPositionDataComponent()
	{
		addDataComponent(new DataComponentVector2f(this, GameObject.POSITION, true, true, false));
	}

	public void addDataComponent(DataComponent dataComponent)
	{
		dataComponent.setDataId(nextDataComponent++);
		dataComponents.add(dataComponent);
	}

	public void removeDataComponent(DataComponent dataComponent)
	{
		dataComponents.add(dataComponent);
	}

	public void addBehaviorComponent(BehaviorComponent behaviorComponent)
	{
		behaviorComponents.add(behaviorComponent);
	}

	public void removeBehaviorComponent(BehaviorComponent behaviorComponent)
	{
		behaviorComponents.add(behaviorComponent);
	}

	public <T> T findDataComponentByClass(Class<T> clazz)
	{
		for (DataComponent component : dataComponents)
		{
			if (component.getClass() == clazz)
				return clazz.cast(component);
		}
		return null;
	}

	public <T> T findBehaviorComponentByClass(Class<T> clazz)
	{
		for (BehaviorComponent component : behaviorComponents)
		{
			if (component.getClass() == clazz)
				return clazz.cast(component);
		}
		return null;
	}

	public <T> T findDataComponentByClassAndName(Class<T> clazz, String name)
	{
		for (DataComponent component : dataComponents)
		{
			if (component.getClass() == clazz && name.equalsIgnoreCase(component.getName()))
				return clazz.cast(component);
		}
		return null;
	}

	public <T> T findBehaviorComponentByClassAndName(Class<T> clazz, String name)
	{
		for (BehaviorComponent component : behaviorComponents)
		{
			if (component.getClass() == clazz && component.getName().equalsIgnoreCase(name))
				return clazz.cast(component);
		}
		return null;
	}

	public DataComponent findDataComponentById(int dataId)
	{
		for (DataComponent component : dataComponents)
		{
			if (component.getDataId() == dataId)
				return component;
		}
		return null;
	}

	public void init()
	{
		for (BehaviorComponent component : behaviorComponents)
		{
			component.init();
		}
	}

	public void update(float delta)
	{

		// Initialize the value fields of replicated components
		for (DataComponent data : dataComponents)
			if (data.isReplicated())
				data.initValueField();

		for (BehaviorComponent component : behaviorComponents)
		{
			if (component.doesUpdate())
				component.update(delta);
		}

		// Replicate game object's components
		for (DataComponent data : dataComponents)
		{
			if (data.canReplicate())
			{
				if (authoritative)
				{
					// Perform replication
					EngineMain.getNetworkManager().replicateComponent(data);
				} else
				{
					// TODO: Perform verification
				}
			}
		}

	}

	/**
	 * Replicates all data components marked for serialization.
	 */
	public void sendSerialized(DataOutput output)
	{
		for (DataComponent data : dataComponents)
			if (data.isSerialized())
				data.send(output);
	}

	/**
	 * Deserializes all data components marked for serialization.
	 */
	public void readSerialized(DataInput input)
	{
		for (DataComponent data : dataComponents)
			if (data.isSerialized())
				data.read(input);
	}

	/**
	 * Replicates all data components marked for replication.
	 */
	public void sendReplicated(DataOutput output)
	{
		for (DataComponent data : dataComponents)
			if (data.isReplicated())
				data.send(output);
	}

	public void readReplicated(DataInput input)
	{
		for (DataComponent data : dataComponents)
			if (data.isReplicated())
				data.read(input);
	}

	public void packet_send_spawn(DataOutput output)
	{
		synchronized (output)
		{
			try
			{
				output.writeInt(Packets.SPAWN);
				output.writeInt(EngineMain.getObjectManager().getGameObjectId(getClass()));
				output.writeInt(id);

				sendSerialized(output);

				// TODO: add event parameters?
				onSendSpawnDelegate.fire(new Event());
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public static GameObject packet_read_spawn(DataInput input)
	{
		try
		{
			int type_id = input.readInt();
			int object_id = input.readInt();
			GameObject object = EngineMain.getObjectManager().makeGameObject(type_id);

			// Only assign ID if it hasn't already been assigned
			if (!object.hasId())
				object.id = object_id;

			System.out.println("id is " + object_id);

			object.readSerialized(input);

			// An object spawned from the network can't be locally owned
			object.ownedLocally = false;

			// TODO: add event parameters?
			object.onReadSpawnDelegate.fire(new Event());

			return object;

		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public void render()
	{
		for (BehaviorComponent component : behaviorComponents)
		{
			if (component.doesRender())
				component.render();
		}
	}

}
