package me.joeyleavell.gdxengine.scene.object;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import me.joeyleavell.gdxengine.scene.GameWorld;
import me.joeyleavell.gdxengine.scene.data.DataComponentBody;
import me.joeyleavell.gdxengine.scene.data.DataComponentVector2f;

public class DynamicBox extends GOBox
{

	public DynamicBox(GameWorld gameWorld)
	{
		super(gameWorld, 10, 100, BodyType.DynamicBody);
	}

	public DynamicBox(GameWorld gameWorld, float w, float h, float x, float y)
	{
		super(gameWorld, w, h, BodyType.DynamicBody);

		findDataComponentByClassAndName(DataComponentVector2f.class, POSITION).value.set(x, y);
		findDataComponentByClassAndName(DataComponentBody.class, BOUNDS).teleport();
	}

}
