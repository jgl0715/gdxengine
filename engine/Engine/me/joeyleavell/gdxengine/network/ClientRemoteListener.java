package me.joeyleavell.gdxengine.network;

import java.io.IOException;

import com.badlogic.gdx.utils.DataInput;

import me.joeyleavell.gdxengine.core.EngineMain;
import me.joeyleavell.gdxengine.scene.data.DataComponent;
import me.joeyleavell.gdxengine.scene.object.GameObject;

public class ClientRemoteListener extends RemoteListener
{

	private Client client;

	public ClientRemoteListener(Client client)
	{
		super(client.getSocketInput(), "Client-Listener");

		this.client = client;
	}

	@Override
	public void packet_spawn(GameObject spawnedObject) throws IOException
	{
		// Spawn a game object

		System.out.println("object with id spawning " + spawnedObject.getId());

		spawnedObject.setLocallyOwned(false);
		spawnedObject.setAuthoritative(true);

		// Tell client the assigned object ID
		synchronized (client.getSocketOutput())
		{
			client.getSocketOutput().writeInt(Packets.ID);
			client.getSocketOutput().writeInt(spawnedObject.getId());
		}

		System.out.println("send assign id");

		// Add the object with replication. Invoking this function on the server will
		// cause the object to be spawned on all clients but the owner to avoid repeat
		// spawning.
		System.out.println("adding object with replication");
		EngineMain.getCurrentWorld().addObjectWithReplication(spawnedObject, client);
	}

	@Override
	public void packet_id(int assignedId) throws IOException
	{
		// client should not be trying to assign id's to the server
	}

	@Override
	public void packet_remove(GameObject objectToRemove) throws IOException
	{
		// TODO: what to do when the client tries to remove an entity?
	}

	@Override
	public void packet_replication(DataComponent component, DataInput input) throws IOException
	{
		component.read(input);

		// TODO: Add verification step since client is replicating to server

		// Multicast replication to other clients if verififed
		EngineMain.getNetworkManager().replicateComponent(component, client);
	}

	@Override
	public void packet_disconnect(DataInput input) throws IOException
	{

	}

	@Override
	public void packet_rpc(String remoteProcedure, Object[] parameters) throws IOException
	{

	}

}
