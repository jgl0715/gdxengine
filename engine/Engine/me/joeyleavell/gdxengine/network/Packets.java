package me.joeyleavell.gdxengine.network;

public class Packets
{

	public static final int REPLICATION = 0x01;
	public static final int SPAWN = 0x02;
	public static final int REMOVE = 0x03;
	public static final int ID = 0x04;
	public static final int RPC = 0x05;
	public static final int DISCONNECT = 0x06;

}
