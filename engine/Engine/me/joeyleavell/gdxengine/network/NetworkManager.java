package me.joeyleavell.gdxengine.network;

import java.io.DataInput;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.utils.DataOutput;

import me.joeyleavell.gdxengine.core.EngineMain;
import me.joeyleavell.gdxengine.scene.data.DataComponent;
import me.joeyleavell.gdxengine.scene.object.GameObject;

public class NetworkManager
{

	private Map<Object, Integer> parameterMappings;
	private Map<String, RemoteProcedure> procedures;
	private ServerManager serverManager;
	private ServerConnection clientManager;
	private boolean server;

	public NetworkManager()
	{
		serverManager = new ServerManager();

		// Start instance as technical server, but don't spawn a server unless
		// explicitly told to do so
		server = true;

		parameterMappings = new HashMap<Object, Integer>();
		parameterMappings.put(Float.class, RemoteProcedure.PARAMETER_FLOAT);
		parameterMappings.put(Boolean.class, RemoteProcedure.PARAMETER_BOOLEAN);
		parameterMappings.put(Double.class, RemoteProcedure.PARAMETER_DOUBLE);
		parameterMappings.put(Integer.class, RemoteProcedure.PARAMETER_INT);
		parameterMappings.put(String.class, RemoteProcedure.PARAMETER_STRING);

		procedures = new HashMap<String, RemoteProcedure>();
	}

	public ServerManager getServerManager()
	{
		return serverManager;
	}

	public ServerConnection getClientManager()
	{
		return clientManager;
	}

	public boolean isServer()
	{
		return server;
	}

	public void runServer(int serverPort)
	{
		// Check if this game instance is already connected to a server
		if (!server && clientManager.isConnected())
		{
			clientManager.disconnectFromServer();
		}

		serverManager.startServer(serverPort);
	}

	public RemoteProcedure getRemoteProcedure(String name)
	{
		return procedures.get(name);
	}

	public void makeRpc(Object object, String remoteProcedureName, RPCType type)
	{
		procedures.put(remoteProcedureName, new RemoteProcedure(object, remoteProcedureName, type));
	}

	public void networkRpc(String remoteProcedureName, Object... parameters)
	{
		try
		{
			RemoteProcedure procedure = procedures.get(remoteProcedureName);

			if (procedure == null)
				throw new IllegalArgumentException("Remote procedure " + procedure + " does not exist");

			if (!procedure.checkParameters(parameters))
				throw new IllegalArgumentException("Parameters are incorrect for this remote procedure");

			RPCType type = procedure.getType();
			DataOutput output = null;
			Integer[] parameterIds = getParameterIds(parameters);

			if (server)
				output = serverManager.getMulticastOutput();
			else
				output = clientManager.getSocketOutput();

			synchronized (output)
			{

				// Multicast RPCs also get executed on the server
				if (server && type == RPCType.MULTICAST)
					procedure.invoke(parameters);

				// [RPC] [Name] [ParameterCount] [ParameterIds] [ParameterValues]
				output.writeInt(Packets.RPC);
				output.writeUTF(remoteProcedureName);
				output.writeInt(procedure.getParameterCount());
				for (Integer i : parameterIds)
					output.writeInt(i);
				for (Object parameter : parameters)
					serializeParameter(parameter, output);
				
				if(server)
					serverManager.multicast();
			}

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private Integer[] getParameterIds(Object... parameters)
	{
		Integer[] result = new Integer[parameters.length];
		for (int i = 0; i < result.length; i++)
			result[i] = getParameterId(parameters[i]);
		return result;
	}

	private Integer getParameterId(Object parameter)
	{
		Integer id = parameterMappings.get(parameter.getClass());
		if (id == null)
			throw new IllegalArgumentException("Parameter does not have an id");
		return id;
	}

	public void serializeParameter(Object parameter, DataOutput output)
	{
		try
		{
			int parameterId = parameterMappings.get(parameter.getClass());
			switch (parameterId)
			{
			case RemoteProcedure.PARAMETER_BOOLEAN:
				output.writeBoolean((Boolean) parameter);
				break;
			case RemoteProcedure.PARAMETER_DOUBLE:
				output.writeDouble((Double) parameter);
				break;
			case RemoteProcedure.PARAMETER_FLOAT:
				output.writeFloat((Float) parameter);
				break;
			case RemoteProcedure.PARAMETER_INT:
				output.writeInt((Integer) parameter);
				break;
			case RemoteProcedure.PARAMETER_STRING:
				output.writeUTF((String) parameter);
				break;
			default:
				break;
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public Object deserializeParameter(int parameterId, DataInput input)
	{
		try
		{
			switch (parameterId)
			{
			case RemoteProcedure.PARAMETER_BOOLEAN:
				return (Boolean) input.readBoolean();
			case RemoteProcedure.PARAMETER_DOUBLE:
				return (Double) input.readDouble();
			case RemoteProcedure.PARAMETER_FLOAT:
				return (Float) input.readFloat();
			case RemoteProcedure.PARAMETER_INT:
				return (Integer) input.readInt();
			case RemoteProcedure.PARAMETER_STRING:
				return (String) input.readUTF();
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public void connectToServer(String serverIp, int serverPort)
	{
		// It is possible for the client to technically be a server at the start, but
		// not actually be running
		// That's why the second check is needed
		if (server && serverManager.isRunning())
			serverManager.stop();

		// Set intance to client
		server = false;

		clientManager = new ServerConnection(serverIp, serverPort);
		if (!clientManager.isConnected())
		{
			// If the client fails to connect to server, default back to running as server
			server = true;
		}
	}

	public synchronized void replicateComponent(DataComponent component)
	{
		replicateComponent(component, null);
	}

	/**
	 * If called on the client, this method will replicate the component to the
	 * server. If called on the server, this method will multicast replicate the
	 * component to all clients.
	 * 
	 * @param component
	 *            The component to replicate
	 */
	public synchronized void replicateComponent(DataComponent component, Client replicatingClient)
	{
		DataOutput output = null;
		if (server)
			output = serverManager.getMulticastOutput();
		else
			output = clientManager.getSocketOutput();

		component.replicate(output);

		if (server)
			serverManager.multicast(replicatingClient);
	}

	public synchronized void spawnOverNetwork(GameObject object, Client spawningClient)
	{
		if (server)
		{
			serverManager.multicastSpawn(object, spawningClient);
		} else
		{
			clientManager.spawnObjectOnServer(object);
		}
	}

	public synchronized void removeOverNetwork(GameObject object)
	{
		removeOverNetwork(object, null);
	}

	public synchronized void removeOverNetwork(GameObject object, Client removingClient)
	{
		int packetId = Packets.REMOVE;
		int objectId = EngineMain.getObjectManager().getGameObjectId(object.getClass());
		DataOutput output = clientManager.getSocketOutput();

		if (server)
			output = serverManager.getMulticastOutput();

		try
		{
			output.writeInt(packetId);
			output.writeInt(objectId);
		} catch (IOException e)
		{
			e.printStackTrace();
		}

		// Multicast the remove to all clients but the owner if this instance is a
		// server
		if (server)
			serverManager.multicast(removingClient);
	}

	public void update(float delta)
	{

	}

	public void dispose()
	{
		// Only needs to stop server if there is an active server instance running
		if (server && serverManager.isRunning())
			serverManager.stop();

		if (!server)
			clientManager.disconnectFromServer();
	}

}
