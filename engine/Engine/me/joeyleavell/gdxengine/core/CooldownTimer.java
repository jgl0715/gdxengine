package me.joeyleavell.gdxengine.core;

public class CooldownTimer
{

	private double lastFire;
	private double interval;

	public CooldownTimer(double intervalSeconds)
	{
		// Convert milliseconds to seconds
		this.interval = intervalSeconds * 1000;
	}

	public double getLastCheck()
	{
		return lastFire;
	}

	public double getInterval()
	{
		return interval;
	}

	public boolean can()
	{
		if (System.currentTimeMillis() - lastFire >= interval)
		{
			lastFire = System.currentTimeMillis();
			return true;
		}
		return false;
	}

}
