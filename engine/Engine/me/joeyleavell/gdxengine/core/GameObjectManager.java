package me.joeyleavell.gdxengine.core;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import me.joeyleavell.gdxengine.scene.GameWorld;
import me.joeyleavell.gdxengine.scene.object.GameObject;

public class GameObjectManager
{

	// Create two maps since it is a guarenteed one to one mapping
	private Map<Integer, Class<? extends GameObject>> gameObjects;
	private Map<Class<? extends GameObject>, Integer> gameObjectsBack;

	public GameObjectManager()
	{
		gameObjects = new HashMap<Integer, Class<? extends GameObject>>();
		gameObjectsBack = new HashMap<Class<? extends GameObject>, Integer>();
	}

	public void addGameObject(int id, Class<? extends GameObject> gameObject)
	{
		gameObjects.put(id, gameObject);
		gameObjectsBack.put(gameObject, id);
	}

	public void removeGameObject(int id)
	{
		gameObjectsBack.remove(gameObjects.remove(id));
	}

	public int getGameObjectId(Class<? extends GameObject> clazz)
	{
		return gameObjectsBack.get(clazz);
	}

	public GameObject makeGameObject(int id)
	{
		try
		{
			Class<? extends GameObject> objectClass = gameObjects.get(id);
			GameWorld currentWorld = EngineMain.getCurrentWorld();
			GameObject newObject = objectClass.getConstructor(GameWorld.class).newInstance(currentWorld);
			return newObject;
		} catch (InstantiationException e)
		{
			e.printStackTrace();
		} catch (IllegalAccessException e)
		{
			e.printStackTrace();
		} catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		} catch (InvocationTargetException e)
		{
			e.printStackTrace();
		} catch (NoSuchMethodException e)
		{
			e.printStackTrace();
		} catch (SecurityException e)
		{
			e.printStackTrace();
		}
		return null;
	}

}