package me.joeyleavell.gdxengine.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.util.vector.Vector2f;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

public class InputManager implements InputProcessor
{

	private List<String> actions;
	private Map<String, List<InputSource>> actionBinds;

	public InputManager()
	{
		actions = new ArrayList<String>();
		actionBinds = new HashMap<String, List<InputSource>>();
	}

	public void registerAction(String actionName)
	{
		if (actions.contains(actionName))
			throw new IllegalStateException("Action " + actionName + " already registered!");
		actions.add(actionName);
		actionBinds.put(actionName, new ArrayList<InputSource>());
	}

	public void registerActionBinding(String action, InputSource input)
	{
		// TODO: make custom exception handling and logging system
		if (!actions.contains(action))
			throw new IllegalStateException("Action " + action + " not registered!");
		else
			actionBinds.get(action).add(input);
	}

	public boolean isActionActivated(String actionName)
	{
		List<InputSource> actionSources = actionBinds.get(actionName);

		// Check all input sources registered with this action.
		for (InputSource source : actionSources)
			if (source.isActivated())
				return true;
		return false;
	}

	public Vector2f getVectorToMouse(int screenX, int screenY)
	{
		return new Vector2f(Gdx.input.getX() - screenX, (Gdx.graphics.getHeight() - Gdx.input.getY()) - screenY);
	}

	public float getMouseAngle(int screenX, int screenY)
	{
		Vector2f mouseVec = getVectorToMouse(screenX, screenY);

		return (float) Math.atan2(mouseVec.y, mouseVec.x);
	}

	public void update(float delta)
	{

	}

	@Override
	public boolean keyDown(int keycode)
	{
		if (keycode == Keys.W)
			System.out.println("I WAS PRESSED");

		return false;
	}

	@Override
	public boolean keyUp(int keycode)
	{
		return false;
	}

	@Override
	public boolean keyTyped(char character)
	{
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button)
	{
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button)
	{
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer)
	{
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY)
	{
		return false;
	}

	@Override
	public boolean scrolled(int amount)
	{
		return false;
	}

}
