package me.joeyleavell.gdxengine.core;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Timer;

import me.joeyleavell.gdxengine.core.InputSource.SourceType;
import me.joeyleavell.gdxengine.network.NetworkManager;
import me.joeyleavell.gdxengine.scene.GameWorld;

public class EngineMain implements ApplicationListener
{
	
	/**
	 * TODO: Add axis binds to the input manager
	 * TODO: Anti cheat
	 * TODO: make movement component not have hard coded axis names
	 * TODO: add physics system so pong game doesnt have to do custom stuff
	 */

	private static Game game;
	private static InputManager inputManager;
	private static ShapeRenderer shapeRenderer;
	private static NetworkManager networkManager;
	private static GameObjectManager objectManager;
	private static GameWorld currentWorld;

	public EngineMain(Game game)
	{
		EngineMain.game = game;
	}

	@Override
	public void create()
	{

		// Create engine subsystems
		networkManager = new NetworkManager();

		objectManager = new GameObjectManager();

		inputManager = new InputManager();
		inputManager.registerAction("Left");
		inputManager.registerAction("Right");
		inputManager.registerAction("Up");
		inputManager.registerAction("Down");
		inputManager.registerActionBinding("Left", new InputSource(SourceType.KEYBOARD, Keys.A));
		inputManager.registerActionBinding("Right", new InputSource(SourceType.KEYBOARD, Keys.D));
		inputManager.registerActionBinding("Up", new InputSource(SourceType.KEYBOARD, Keys.W));
		inputManager.registerActionBinding("Down", new InputSource(SourceType.KEYBOARD, Keys.S));

		shapeRenderer = new ShapeRenderer();

		game.create();
	}

	public static void repeatTask(RepeatingTask task)
	{
		Timer.schedule(task, 0, task.getIntervalSeconds());
	}

	public static InputManager getInputManager()
	{
		return inputManager;
	}

	public static ShapeRenderer getShapeRenderer()
	{
		return shapeRenderer;
	}

	public static NetworkManager getNetworkManager()
	{
		return networkManager;
	}

	public static GameObjectManager getObjectManager()
	{
		return objectManager;
	}

	public static GameWorld getCurrentWorld()
	{
		return currentWorld;
	}

	public static void setCurrentWorld(GameWorld currentWorld)
	{
		EngineMain.currentWorld = currentWorld;
	}

	public static Game getGame()
	{
		return game;
	}

	@Override
	public void resize(int width, int height)
	{

	}

	@Override
	public void render()
	{
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// Update
		float delta = Gdx.graphics.getDeltaTime();
		
		if (currentWorld != null)
			currentWorld.update(delta);
		
		game.update(delta);

		// Render

		if (currentWorld != null)
			currentWorld.render();

		game.render();
	}

	@Override
	public void pause()
	{

	}

	@Override
	public void resume()
	{

	}

	@Override
	public void dispose()
	{
		EngineMain.game.dispose();
		EngineMain.networkManager.dispose();
	}

}
