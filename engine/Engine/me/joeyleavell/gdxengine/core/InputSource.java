package me.joeyleavell.gdxengine.core;

import com.badlogic.gdx.Gdx;

public class InputSource
{

	public enum SourceType
	{
		KEYBOARD, MOUSE;
	}

	private SourceType source;
	private int id;

	public InputSource(SourceType source, int id)
	{
		this.source = source;
		this.id = id;
	}

	public SourceType getSource()
	{
		return source;
	}

	public int getId()
	{
		return id;
	}

	public boolean isActivated()
	{
		switch(source)
		{
		case KEYBOARD:
			return Gdx.input.isKeyPressed(id);
		case MOUSE:
			return Gdx.input.isButtonPressed(id);
		default:
			return false;
		}
	}

}
