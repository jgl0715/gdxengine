package me.joeyleavell.gdxengine.core;

public class RenderSettings
{

	public boolean visible;
	public RenderMode mode;

	public enum RenderMode
	{
		TEXTURED, FRAME
	}

	public RenderSettings()
	{
		visible = true;
		mode = RenderMode.FRAME;
	}

}
